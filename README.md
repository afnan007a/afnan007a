**Hello, homies!** <img src="https://raw.githubusercontent.com/Jaydo-Coder/Jaydo-Coder/main/wave.gif" width="30px">

> I am Afnan K Salal.
>
> I am 15 years old and I'm from India. 
>
> I am a Full-Stack Developer, a System Administrator and an Animator. 
>
> I am currently working at VerdisHost and ArkNodes
> 
> My latest projects are Mugiwara, Rhombus Panel and StockBase

**Qualifications**

> I have experience in Hosting Industry, GTA Role Play Servers, Minecraft Networks from late 2017.
>
> I am experienced with WHMCS , Blesta, Direct Admin, Pterodactyl, Multicraft, Cpanel, AApanel, and more. 
>
> I'm also good at managing Linux VPS, Cloud Servers, AWS and GCP dashboard (EC2, Lightsail, S3, etc) , Database, Docker, K8s etc. 
>
> I'm also good at coding in JS (React, Next, Vue, DJS, EJS), Python (Django, Flask, Sanic, DPY, Hikari, etc), Shell scripting. 
>
> I am also good at FanArts, 2D/3D Animation, Poster/advertisement creation, Video editing, etc.
> 
> And finally I am also good at Marketing and Customer care.

**Miscellaneous**

> Portfolio: https://me.redknight.xyz
>
> Email: anonymous@terrorist.lol
>
> Discord: Afnan#2316


**Thats all, Have a good day! 🥰**
